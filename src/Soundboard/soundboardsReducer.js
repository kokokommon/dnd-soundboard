import { soundboardsActions } from "./soundboardsActions";

export default function soundboardsReducer(state = [], action) {
  switch (action.type) {
    case soundboardsActions.GET_SOUNDBOARDS_SUCCESS:
      return action.payload;
    case soundboardsActions.POST_SOUNDBOARD_SUCCESS:
      return state.concat([action.payload])
    case soundboardsActions.DELETE_SOUNDBOARD_SUCCESS:
      return state.filter(soundboard => soundboard.id !== action.payload)
    default:
      return state
  }
}