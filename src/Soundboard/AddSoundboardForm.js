import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addSoundboard } from './soundboardsActions';

export class AddSoundboardForm extends Component {

  
  resetState() {
    this.setState({});
  }

  onInputChange(event) {
    let soundboardName = event.target.value;
    this.setState(state => {
      return { ...state, soundboardName }
    })
  }

  onSaveSoundboard() {
    const soundboard = {
      name: this.state.soundboardName,
    }
    this.props.dispatch(addSoundboard(soundboard))
  }

  componentWillMount() {
    this.setState((state) => {
      return { ...state, soundboardName: '' }
    })
  }

  render() {


    return (
      <div>

        <form onSubmit={this.onSaveSoundboard.bind(this)}>
          <label>
            Name:
            <input type="text" value={this.state.soundboardName} onChange={this.onInputChange.bind(this)} />
          </label>
          <button type="submit" value="Submit">Save soundboard</button>
        </form>

      </div>
    ) 
  }

}

export default connect()(AddSoundboardForm)