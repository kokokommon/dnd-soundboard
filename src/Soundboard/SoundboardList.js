import React, { Component } from 'react';
import SoundboardTile from "./SoundboardTile";


export default class SoundboardList extends Component {

  render() {

    return (
      <div>
        { this.props.soundboards && this.props.soundboards.map(soundboard => {
          return <SoundboardTile key={soundboard.id} soundboard={soundboard}/>
        })}
      </div>
    ) 
  }

}