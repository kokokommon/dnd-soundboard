import { soundboardsActions } from "./soundsoundboardsActions";

export default function soundboardsReducer(state = [], action) {
  switch (action.type) {
    case soundboardsActions.GET_soundboardS_SUCCESS:
      return action.payload;
    case soundboardsActions.POST_soundboard_SUCCESS:
      return state.concat([action.payload])
    case soundboardsActions.DELETE_soundboard_SUCCESS:
      return state.filter(soundboard => soundboard.id !== action.payload)
    default:
      return state
  }
}