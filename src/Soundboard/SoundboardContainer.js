import React, { Component } from 'react';
import { connect } from 'react-redux';
import SoundboardList from './SoundboardList';
import { fetchSoundboards } from '../Soundboard/soundboardsActions'
import AddSoundboardForm from './AddSoundboardForm';

export class SoundboardContainer extends Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.dispatch(fetchSoundboards())
  }

  render() {
    return (
      <div>
        <SoundboardList soundboards={this.props.soundboards} />
        <AddSoundboardForm/>
      </div>
    )
  }

}

const mapStateToProps = state => {
  return {
    soundboards: state.soundboards
  }
}

export default connect(
  mapStateToProps,
)(SoundboardContainer)