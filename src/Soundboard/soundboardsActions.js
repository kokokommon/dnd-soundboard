import db from "../app/indexedDbInitializer";

export const soundboardsActions = {
  GET_SOUNDBOARDS: 'GET_SOUNDBOARDS',
  GET_SOUNDBOARDS_SUCCESS: 'GET_SOUNDBOARDS_SUCCESS',
  GET_SOUNDBOARDS_ERROR: 'GET_SOUNDBOARDS_ERROR',
  POST_SOUNDBOARD: 'POST_SOUNDBOARD',
  POST_SOUNDBOARD_SUCCESS: 'POST_SOUNDBOARD_SUCCESS',
  POST_SOUNDBOARD_ERROR: 'POST_SOUNDBOARD_ERROR',
  DELETE_SOUNDBOARD: 'DELETE_SOUNDBOARD',
  DELETE_SOUNDBOARD_SUCCESS: 'DELETE_SOUNDBOARD_SUCCESS',
  DELETE_SOUNDBOARD_ERROR: 'DELETE_SOUNDBOARD_ERROR',
}

function getSoundboards() {
  return {
    type: soundboardsActions.GET_SOUNDBOARDS
  }
}

function getSoundboardsSuccess(soundboards) {
  return {
    type: soundboardsActions.GET_SOUNDBOARDS_SUCCESS,
    payload: soundboards
  }
}

function getSoundboardsError(error) {
  return {
    type: soundboardsActions.GET_SOUNDBOARDS_ERROR,
    payload: error
  }
}

export function fetchSoundboards() {
  return function(dispatch) {
    dispatch(getSoundboards())
    return db.soundboards.toArray().then(
      response => dispatch(getSoundboardsSuccess(response)), 
      error => dispatch(getSoundboardsError(error)))
  }
}

function postSoundboard(soundboard) {
  return {
    type: soundboardsActions.POST_SOUNDBOARD,
    payload: soundboard
  }
}

function postSoundboardsuccess(soundboard) {
  return {
    type: soundboardsActions.POST_SOUNDBOARD_SUCCESS,
    payload: soundboard
  }
}

function postSoundboardError(error) {
  return {
    type: soundboardsActions.POST_SOUNDBOARD_ERROR,
    payload: error
  }
}

export function addSoundboard(soundboard) {
  return function(dispatch) {
    dispatch(postSoundboard(soundboard))
    return db.soundboards.add(soundboard).then(
      id => dispatch(postSoundboardsuccess({ ...soundboard, id })),
      error => dispatch(postSoundboardError(error)))
  }
}

function deleteSoundboard(id) {
  return {
    type: soundboardsActions.DELETE_SOUNDBOARD,
    payload: id
  }
}

function deleteSoundboardsuccess(id) {
  return {
    type: soundboardsActions.DELETE_SOUNDBOARD_SUCCESS,
    payload: id
  }
}

function deleteSoundboardError(error) {
  return {
    type: soundboardsActions.DELETE_SOUNDBOARD_ERROR,
    payload: error
  }
}

export function removeSoundboard(id) {
  return function(dispatch) {
    dispatch(deleteSoundboard(id))
    return db.soundboards.delete(id).then(
      () => dispatch(deleteSoundboardsuccess(id)),
      error => dispatch(deleteSoundboardError(error)))
  }
}