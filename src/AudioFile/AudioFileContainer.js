import React, { Component } from 'react';
import { connect } from 'react-redux';
import AudioFileList from './AudioFileList';
import { fetchAudioFiles } from '../AudioFile/audioFilesActions'
import AddAudioFileForm from './AddAudioFileForm';

export class AudioFileContainer extends Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.dispatch(fetchAudioFiles())
  }

  render() {
    return (
      <div>
        <AudioFileList audioFiles={this.props.audioFiles} />
        <AddAudioFileForm/>
    </div>
    )
  }

}

const mapStateToProps = state => {
  return {
    audioFiles: state.audioFiles
  }
}

export default connect(
  mapStateToProps,
)(AudioFileContainer)