import React, { Component } from 'react';
import { connect } from 'react-redux';
import { removeAudioFile } from './audioFilesActions';
import AudioFilePlayer from './AudioFilePlayer';

export class AudioFile extends Component {

  onClick() {
    this.props.dispatch(removeAudioFile(this.props.audioFile.id))
  }

  render() {
    return (
      <div>
        <span>{ this.props.audioFile.id } / </span>
        <b>{ this.props.audioFile.name } </b>
        <AudioFilePlayer file={this.props.audioFile.blob} />
        <button onClick={this.onClick.bind(this)}>Delete</button>
      </div>
    ) 
  }

}

export default connect()(AudioFile)
