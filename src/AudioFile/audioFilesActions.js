import db from "../app/indexedDbInitializer";

export const audioFilesActions = {
  GET_AUDIO_FILES: 'GET_AUDIO_FILES',
  GET_AUDIO_FILES_SUCCESS: 'GET_AUDIO_FILES_SUCCESS',
  GET_AUDIO_FILES_ERROR: 'GET_AUDIO_FILES_ERROR',
  POST_AUDIO_FILE: 'POST_AUDIO_FILE',
  POST_AUDIO_FILE_SUCCESS: 'POST_AUDIO_FILE_SUCCESS',
  POST_AUDIO_FILE_ERROR: 'POST_AUDIO_FILE_ERROR',
  DELETE_AUDIO_FILE: 'DELETE_AUDIO_FILE',
  DELETE_AUDIO_FILE_SUCCESS: 'DELETE_AUDIO_FILE_SUCCESS',
  DELETE_AUDIO_FILE_ERROR: 'DELETE_AUDIO_FILE_ERROR',
}

function getAudioFiles() {
  return {
    type: audioFilesActions.GET_AUDIO_FILES
  }
}

function getAudioFilesSuccess(audioFiles) {
  return {
    type: audioFilesActions.GET_AUDIO_FILES_SUCCESS,
    payload: audioFiles
  }
}

function getAudioFilesError(error) {
  return {
    type: audioFilesActions.GET_AUDIO_FILES_ERROR,
    payload: error
  }
}

export function fetchAudioFiles() {
  return function(dispatch) {
    dispatch(getAudioFiles())
    return db.audioFiles.toArray().then(
      response => dispatch(getAudioFilesSuccess(response)), 
      error => dispatch(getAudioFilesError(error)))
  }
}

function postAudioFile(audioFile) {
  return {
    type: audioFilesActions.POST_AUDIO_FILE,
    payload: audioFile
  }
}

function postAudioFileSuccess(audioFile) {
  return {
    type: audioFilesActions.POST_AUDIO_FILE_SUCCESS,
    payload: audioFile
  }
}

function postAudioFileError(error) {
  return {
    type: audioFilesActions.POST_AUDIO_FILE_ERROR,
    payload: error
  }
}

export function addAudioFile(audioFile) {
  return function(dispatch) {
    dispatch(postAudioFile(audioFile))
    return db.audioFiles.add(audioFile).then(
      id => dispatch(postAudioFileSuccess({ ...audioFile, id })),
      error => dispatch(postAudioFileError(error)))
  }
}

function deleteAudioFile(id) {
  return {
    type: audioFilesActions.DELETE_AUDIO_FILE,
    payload: id
  }
}

function deleteAudioFileSuccess(id) {
  return {
    type: audioFilesActions.DELETE_AUDIO_FILE_SUCCESS,
    payload: id
  }
}

function deleteAudioFileError(error) {
  return {
    type: audioFilesActions.DELETE_AUDIO_FILE_ERROR,
    payload: error
  }
}

export function removeAudioFile(id) {
  return function(dispatch) {
    dispatch(deleteAudioFile(id))
    return db.audioFiles.delete(id).then(
      () => dispatch(deleteAudioFileSuccess(id)),
      error => dispatch(deleteAudioFileError(error)))
  }
}