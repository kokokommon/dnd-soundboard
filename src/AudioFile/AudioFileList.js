import React, { Component } from 'react';
import AudioFile from "./AudioFile";


export default class AudioFileList extends Component {

  render() {

    return (
      <div>
        { this.props.audioFiles && this.props.audioFiles.map(audioFile => {
          return <AudioFile key={audioFile.id} audioFile={audioFile}/>
        })}
      </div>
    ) 
  }

}