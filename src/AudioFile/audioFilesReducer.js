import { audioFilesActions } from "./audioFilesActions";

export default function audioFilesReducer(state = [], action) {
  switch (action.type) {
    case audioFilesActions.GET_AUDIO_FILES_SUCCESS:
      return action.payload;
    case audioFilesActions.POST_AUDIO_FILE_SUCCESS:
      return state.concat([action.payload])
    case audioFilesActions.DELETE_AUDIO_FILE_SUCCESS:
      return state.filter(audioFile => audioFile.id !== action.payload)
    default:
      return state
  }
}