import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addAudioFile } from './audioFilesActions';

export class AddAudioFileForm extends Component {

  startRecording() {
    
    navigator.mediaDevices.getUserMedia({ audio: true })
    .then(stream => {
      const mediaRecorder = new MediaRecorder(stream);

      this.setState((state) => {
        return { ...state, mediaRecorder, recordingState: 'recording' };
      })

      mediaRecorder.start();

      const audioChunks = [];

      mediaRecorder.addEventListener("dataavailable", event => {
        audioChunks.push(event.data);
        this.setState((state) => {
          return { ...state, audioChunks}
        })
      });

      mediaRecorder.addEventListener("stop", () => {
        const audioBlob = new Blob(audioChunks);
        const audioUrl = URL.createObjectURL(audioBlob);
        const audio = new Audio(audioUrl);

        this.setState(state => {
          return { ...state, audio, audioBlob }
        })
      });

    });
  }

  resetState() {
    this.setState({});
  }

  onPauseRecording() {
    if (this.state.mediaRecorder.state === 'recording') {
      this.state.mediaRecorder.pause();
      this.setState(state => {
        return { ...state, recordingState: 'paused'}
      })
    }
  }

  onResumeRecording() {
    if (this.state.mediaRecorder.state === 'paused') {
      this.state.mediaRecorder.resume();
      this.setState(state => {
        return { ...state, recordingState: 'recording'}
      })
    }
  }

  onStopRecording() {
    if (this.state.mediaRecorder.state === 'recording' || this.state.mediaRecorder.state === 'paused') {
      this.state.mediaRecorder.stop();
      this.setState(state => {
        return { ...state, recordingState: 'inactive'}
      })
    }
  }

  onPlayAudio() {
    if (this.state.audio) {
      this.state.audio.play();      
    }
  }

  onStopAudio() {
    if (this.state.audio) {
      this.state.audio.pause();      
    }
  }

  onInputChange(event) {
    let audioFileName = event.target.value;
    this.setState(state => {
      return { ...state, audioFileName }
    })
  }

  onSaveAudioFile() {
    const audioFile = {
      name: this.state.audioFileName,
      blob: this.state.audioBlob
    }
    this.props.dispatch(addAudioFile(audioFile))
  }

  componentWillMount() {
    this.setState((state) => {
      return { ...state, audioChunks: [], audioFileName: '' }
    })
  }

  render() {

    return (
      <div>
        <button onClick={this.startRecording.bind(this)}>Record audio</button>
        <p>{ this.state.audioChunks && this.state.audioChunks.length }</p>

        <p>{ this.state.recordingState }</p>

        { this.state.mediaRecorder && this.state.recordingState === 'recording' && <button onClick={this.onPauseRecording.bind(this)}>Pause Recording</button> }
        { this.state.mediaRecorder && this.state.recordingState === 'paused' && <button onClick={this.onResumeRecording.bind(this)}>Resume Recording</button> }
        { this.state.mediaRecorder && this.state.recordingState !== 'inactive' && <button onClick={this.onStopRecording.bind(this)}>Stop Recording</button> }

        <hr></hr>

        { this.state.audio && <button onClick={this.onPlayAudio.bind(this)}>Play</button> }
        { this.state.audio && <button onClick={this.onStopAudio.bind(this)}>Stop</button> }

        <form onSubmit={this.onSaveAudioFile.bind(this)}>
          <label>
            Name:
            <input type="text" value={this.state.audioFileName} onChange={this.onInputChange.bind(this)} />
          </label>
          <button type="submit" value="Submit">Save audio</button>
        </form>

      </div>
    ) 
  }

}

export default connect()(AddAudioFileForm)