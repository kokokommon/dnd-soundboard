import React, { Component } from 'react';

export default class AudioFilePlayer extends Component {

  constructor(props) {
    super(props);
    this.player = new Audio();
  }

  onClick() {
  }

  componentWillMount() {
    if (this.props.file) {
      this.player.src = window.URL.createObjectURL(this.props.file);
    }
  }

  onPlay() {
    if (this.player && this.player.src && this.props.file) {
      this.player.play()
    }
  }

  onPause() {
    if (this.player && this.player.src && this.props.file) {
      this.player.pause()
    }
  }

  render() {
    return (
      <div>
        <button onClick={this.onPlay.bind(this)}>
          Play
        </button>
        <button onClick={this.onPause.bind(this)}>
          Pause
        </button>
      </div>
    ) 
  }

}
