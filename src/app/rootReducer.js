import { combineReducers } from 'redux';
import audioFilesReducer from '../AudioFile/audioFilesReducer';
import soundboardsReducer from '../Soundboard/soundboardsReducer';

export default combineReducers({
  audioFiles: audioFilesReducer,
  soundboards: soundboardsReducer
})