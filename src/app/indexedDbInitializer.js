import Dexie from 'dexie';

const db = new Dexie('DnDSoundBoardDB');

// Version 1
db.version(1).stores({
  audioFiles: '++id, name'
})

// Version 2
db.version(2).stores({
  soundboards: '++id, name'
})

export default db;