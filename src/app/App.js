import React, { Component } from 'react';
import SoundboardItemContainer from '../SoundboardItem/SoundboardItemContainer';
import AudioFileContainer from '../AudioFile/AudioFileContainer';
import SoundboardContainer from '../Soundboard/SoundboardContainer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <SoundboardContainer/>
        <SoundboardItemContainer/>
        <AudioFileContainer/>
      </div>
    );
  }
}

export default App;
