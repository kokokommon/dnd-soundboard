import React, { Component } from 'react';

export default class SoundboardItem extends Component {

  constructor(props) {
    super(props);
    this.audio = new Audio(this.props.audioItem.src);
    this.playSound = this.playSound.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
  }

  playSound() {
    this.audio = new Audio(this.props.audioItem.src);
    this.audio.play();
  }

  onKeyDown(e) {
    if (this.props.audioItem.keys.some(key => key === e.which)) {
      this.playSound();
    }
  }

  render() {
    return (
      <div id={this.props.audioItem.id} ref={elem => this.comp = elem}>
        <h3>{this.props.audioItem.name}</h3>
        <p>{this.props.audioItem.description}</p>
      </div>
    ) 
  }

}