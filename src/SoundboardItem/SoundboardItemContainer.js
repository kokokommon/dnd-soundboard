import SoundboardItem from './SoundboardItem';
import React, { Component } from 'react';

export default class SoundboardItemContainer extends Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    document.addEventListener('keydown', this.onKeyDown.bind(this));
  }

  componentWillUnmount() {
    document.removeEventListener('keydown');
  }

  onKeyDown(e) {
    Object.keys(this.refs).forEach(ref => {
      if (ref !== 'self') {
        this.refs[ref].onKeyDown(e);
      }
    })
  }

  render() {

    const audioItems = [
      {
        id: '76cadc3c0c8d4943b2983751da085f6e',
        name: 'Melee hit',
        description: '',
        keys: [83],
        src: 'audio/sword.mp3',
      },
      {
        id: '7238744e2ba74fbfafe248936c339e59',
        name: 'Bow shot',
        description: '',
        keys: [66],
        src: 'audio/bow.mp3',
      },
    ]

    return (
      <div>
        <h2>SoundboardItem Container:</h2>
        { audioItems && audioItems.map((audioItem, i) => {
          return (
            <SoundboardItem 
              key={i} 
              audioItem={ audioItem } 
              ref={ audioItem.id } 
            />
          )
        })}
      </div>
    )
  }

}